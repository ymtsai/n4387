CXX ?= g++
CXXFLAGS += -std=c++11

all: n4387

n4387: n4387.cpp
	$(CXX) n4387.cpp -o n4387 $(CXXFLAGS)
